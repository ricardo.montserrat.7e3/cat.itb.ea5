import com.google.gson.Gson;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.Scanner;

public class Main
{
    private void pause()
    {
        System.out.println("\n\nPress enter to continue...");
        new Scanner(System.in).nextLine();
    }

    private String inputAny()
    {
        return new Scanner(System.in).nextLine();
    }

    private void ex2()
    {
        Gson json = new Gson();

        try (BufferedReader br = new BufferedReader(new FileReader("products.json")))
        {
            String linea;
            while ((linea = br.readLine()) != null) { System.out.println(json.fromJson(linea, Product.class));}
        }
        catch (Exception ex) {System.out.println(ex.toString());}
    }

    private void ex3()
    {
        Gson json = new Gson();

        Product[] products = {
                new Product("Lenovo Laptop", 1200, 50, "lenovo.png", new Category[]{
                        new Category(1,"technology"), new Category(2,"cheap")}),
                new Product("Minecraft Game", 30, 1000, "minecraft.jpeg", new Category[]{
                        new Category(3,"gaming"), new Category(4,"videogames")}),
                new Product("Iphone 4", 100, 5, "iphone4.png", new Category[]{
                        new Category(5,"old"), new Category(6,"good")}),
                new Product("Android Gaming", 500, 100, "android.png", new Category[]{
                        new Category(7,"lol"), new Category(8,"dreaming")}),
                new Product("IOS Mac", 1200, 10000, "mac.png", new Category[]{
                        new Category(9,"scam"), new Category(10,"expensive")}),
        };

        try(FileWriter fr = new FileWriter("productsData.json"))
        {
            for (Product product : products)
            {
                fr.write(json.toJson(product) + "\n");
            }
            System.out.println("File successfully created!");
        }catch (Exception e){System.out.println(e.toString());}
    }

    private void showMenu()
    {
        System.out.println("---------- Welcome to my Program ----------\n\n" +
                "1.- ex2(Read products.json)\n" +
                "2.- ex3(Create productsData.json with 5 new Products)\n" +
                "3.- exit\n");
    }

    private boolean getInputMenu()
    {
        System.out.print("Select an option by number or name: ");
        switch (inputAny())
        {
            case "ex2": case "1": ex2(); pause();
            break;
            case "ex3": case "2": ex3(); pause(); break;
            case "exit": case "3": return true;
        }
        return false;
    }

    private void start()
    {
        boolean finish;
        do
        {
            showMenu();
            finish = getInputMenu();
        }
        while (!finish);
        System.out.println("Goodbye!! :D");
    }

    public static void main(String[] args)
    {
        Main program = new Main();
        program.start();
    }
}

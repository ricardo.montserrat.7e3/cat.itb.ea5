package ClassesJAVA_JSON.ej4;

import java.util.ArrayList;

public class Persona {

    private String nombre;
    private String apellidos;
    private int edad;
    
    private Asignatura[] asignaturas;

    public Persona(String nombre, String apellidos, int edad) {
        this.nombre = nombre;
        this.apellidos = apellidos;
        this.edad = edad;
    }

    public String getNombre() {
        return nombre;
    }

    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    public String getApellidos() {
        return apellidos;
    }

    public void setApellidos(String apellidos) {
        this.apellidos = apellidos;
    }

    public int getEdad() {
        return edad;
    }

    public void setEdad(int edad) {
        this.edad = edad;
    }

    @Override
    public String toString() {
        String result = "";
        for (Asignatura asig:asignaturas)
        {
            result += asig.getId()+", "+asig.getNombre() + " | ";
        }
        return "{" + "nombre=" + nombre + ", apellidos=" + apellidos + ", edad=" + edad + ", asignaturas=" + result + '}';
    }

    
}

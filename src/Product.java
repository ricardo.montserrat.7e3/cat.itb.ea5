public class Product
{
    private String name, picture;
    private int price, stock;
    private Category[] categories;

    public Product(String name, int price, int stock, String picture, Category[] categories)
    {
        this.name = name;
        this.picture = picture;
        this.price = price;
        this.stock = stock;
        this.categories = categories;
    }

    @Override
    public String toString()
    {
        String product = name + ", Price = " + price + ", on Stock = " + stock + ", Picture = " + picture + ", Categories = ";
        for (Category categor: categories)
        {
            product += categor.toString();
        }
        return product;
    }
}
